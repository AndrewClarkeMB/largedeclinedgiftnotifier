# LargeDeclinedGiftNotifier

Java program for notifying people by email when gifts were declined for being too large.

See https://confluence.ministrybrands.com/display/ET/Large+gifts+decline+notification for usage details.