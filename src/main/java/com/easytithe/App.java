package com.easytithe;

import com.sendgrid.*;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.sql.*;

import java.util.Properties;

/**
 * Hello world!
 */
public class App {
    Properties properties = null;

    public static void main(String[] args) {
        App app = new App();
        app.handleDeclinedGifts();
    }

    private void handleDeclinedGifts() {
        getProperties();
        Connection connection = null;

        try {
            String connectionUrl = "jdbc:sqlserver://" +
                    properties.getProperty("datasource.url") +
                    ":" +
                    properties.getProperty("datasource.port") +
                    ";databaseName=" +
                    properties.getProperty("datasource.name") +
                    ";user=" + properties.getProperty("datasource.user") +
                    ";password=" + properties.getProperty("datasource.password");

            connection = DriverManager.getConnection(connectionUrl);
            String sql = properties.getProperty("datasource.sql");
            Statement statement = connection.createStatement();
            ResultSet resultSet = statement.executeQuery(sql);

            if (! resultSet.isBeforeFirst()) {
                System.out.println("No data found this time.  Not sending an email.");
                return;
            }

            sendEmail(resultSet);
        } catch (Exception e) {
            System.out.println();
            e.printStackTrace();
        } finally {
            try {
                if ((connection != null) && (! connection.isClosed())) connection.close();
            } catch (Exception e) {
                System.out.println("Exception closing connection");
            }
        }
    }

    private void sendEmail(ResultSet resultSet) throws Exception {
        String body = generateMailContent(resultSet);
        getProperties();
        Email from = new Email(properties.getProperty("mail.smtp.from"));
        String subject = properties.getProperty("mail.smtp.subject");
        Email to = new Email(properties.getProperty("mail.smtp.to"));

        Content content = new Content("text/html", body);
        Mail mail = new Mail(from, subject, to, content);

        SendGrid sg = new SendGrid(properties.getProperty("mail.smtp.sendgrid.apikey"));
        Request request = new Request();

        try {
            request.setMethod(Method.POST);
            request.setEndpoint("mail/send");
            request.setBody(mail.build());
            Response response = sg.api(request);
            System.out.println(response.getStatusCode());
            System.out.println(response.getBody());
            System.out.println(response.getHeaders());
        } catch (IOException ex) {
            throw ex;
        }
    }

    private String generateMailContent(ResultSet resultSet) throws Exception {
        getProperties();
        ResultSetMetaData resultSetMetaData = resultSet.getMetaData();
        StringBuilder sb = new StringBuilder(properties.getProperty("mail.smtp.body"));
        int columnCount = resultSetMetaData.getColumnCount();
        sb.append("<br /><br /><table border='2'><tr>");

        for (int i = 1; i <= columnCount; i++) {
            sb.append("<th>").append(resultSetMetaData.getColumnName(i)).append("</th>");
        }

        sb.append("</tr>");

        while (resultSet.next()) {
            sb.append("<tr>");

            for (int i = 1; i <= columnCount; i++) {
                sb.append("<td>");
                if (resultSet.getString(i) != null) sb.append(resultSet.getString(i));
                sb.append("</td>");
            }

            sb.append("</tr>");
        }

        sb.append("</table>");
        String body = sb.toString();
        System.out.println(body);
        return body;
    }

    private void getProperties() {
        if (properties == null) {
            try {
                properties = new Properties();
                File file = new File("setup.properties");
                FileInputStream fileInput = new FileInputStream(file);
                properties.load(fileInput);
                fileInput.close();
            } catch (Exception e) {
                System.out.println("Problem reading setup.properties: " + e.getLocalizedMessage());
            }
        }
    }
}